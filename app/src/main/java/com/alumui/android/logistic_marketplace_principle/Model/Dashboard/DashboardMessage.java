package com.alumui.android.logistic_marketplace_principle.Model.Dashboard;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kristoforus Gumilang on 8/17/2017.
 */

public class DashboardMessage {
    @SerializedName("data")
    public DashboardData data;
}
