package com.alumui.android.logistic_marketplace_principle.Model.Principle;

import com.google.gson.annotations.SerializedName;

/**
 * Created by davidwibisono on 8/24/17.
 */

public class PrincipleContactPersonData {
    @SerializedName("principle")
    public String principle;
    @SerializedName("nama")
    public String name;
    @SerializedName("telp")
    public String phone;
}
