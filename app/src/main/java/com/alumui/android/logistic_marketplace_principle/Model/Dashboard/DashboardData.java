package com.alumui.android.logistic_marketplace_principle.Model.Dashboard;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kristoforus Gumilang on 8/17/2017.
 */

public class DashboardData {
    @SerializedName("statistic")
    public List<DashboardStatistic> statistic;
}
