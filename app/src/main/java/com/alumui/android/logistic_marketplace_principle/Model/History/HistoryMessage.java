package com.alumui.android.logistic_marketplace_principle.Model.History;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kristoforus Gumilang on 8/19/2017.
 */

public class HistoryMessage {
    @SerializedName("data")
    public HistoryData data;
}
