package com.alumui.android.logistic_marketplace_principle.Model.Vendor;

import com.google.gson.annotations.SerializedName;

/**
 * Created by davidwibisono on 8/23/17.
 */

public class VendorData {
    @SerializedName("nama")
    public String name;
    @SerializedName("telp")
    public String phone;
    @SerializedName("alamat")
    public String address;


}
