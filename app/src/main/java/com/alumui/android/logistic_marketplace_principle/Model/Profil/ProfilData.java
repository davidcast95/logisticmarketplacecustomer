package com.alumui.android.logistic_marketplace_principle.Model.Profil;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kristoforus Gumilang on 8/17/2017.
 */

public class ProfilData {
    @SerializedName("gender")
    public String gender;

    @SerializedName("customer_group")
    public String customer_group;

    @SerializedName("customer_name")
    public String customer_name;

    @SerializedName("name")
    public String name;
}
