package com.alumui.android.logistic_marketplace_principle.Model.JobOrder;

import com.google.gson.annotations.SerializedName;

/**
 * Created by davidwibisono on 8/31/17.
 */

public class JobOrderCreationResponse {
    @SerializedName("data")
    JobOrderData newData;

}
