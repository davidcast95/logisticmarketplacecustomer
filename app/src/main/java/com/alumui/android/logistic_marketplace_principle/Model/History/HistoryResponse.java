package com.alumui.android.logistic_marketplace_principle.Model.History;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kristoforus Gumilang on 8/19/2017.
 */

public class HistoryResponse {
    @SerializedName("message")
    public HistoryMessage message;
}
