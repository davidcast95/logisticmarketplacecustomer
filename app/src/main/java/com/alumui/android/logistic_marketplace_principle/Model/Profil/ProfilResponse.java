package com.alumui.android.logistic_marketplace_principle.Model.Profil;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kristoforus Gumilang on 8/17/2017.
 */

public class ProfilResponse {
    @SerializedName("message")
    public ProfilMessage message;
}
